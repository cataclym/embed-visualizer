import React from 'react'
import CopyToClipboard from 'react-copy-to-clipboard';

class Clipboard extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      viewType: 0,
      viewCommand: "Name"
    }
  }

  render(){
    const typeTabClasses = "type-tab whitney";

    return (
        <div className="cmd-name auto-height">
          <input
            onFocus={(e) => e.target.select()}
            className="cmd-action input"
            hidden={this.state.viewType === 0}
            defaultValue={this.state.viewCommand}
            type="text" placeholder="Name"
            onChange={(e) => this.setState({viewCommand: e.target.value})} />
          <CopyToClipboard text={this.props.value}>
            <button className={`${typeTabClasses} border-radius5 eb-add`}>Copy 📋</button>
          </CopyToClipboard>
        </div>
    )}
}

export default Clipboard
